<?php
/**
 * @file
 * Domain Country API form to assign countries to domains.
 */

/**
 * Admin Settings Form building function.
 */
function domain_country_settings() {
  $form['settings'] = array(
    '#title' => t('Settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['default_domain_name'] = array(
    '#title' => t('Default domain region name'),
    '#type' => 'textfield',
    '#description' => t('The default domain has the ID of 0, and cannot be changed. Please specify its name. To change the region names of other domains, edit the domains directly.'),
    '#default_value' => variable_get('tek_client_geoip_default_domain_name', 'International'),
  );
  $form['country_region_mapping'] = array(
    '#title' => t('Country to Region Mapping'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Choose which region a user will be directed to when they choose a country from the country selector.'),
  );
  $domain_options = array();
  $result = db_query('SELECT DISTINCT region_name, domain_id FROM {domain_country} ORDER BY domain_id');
  while ($domain = db_fetch_object($result)) {
    $domain_options[$domain->domain_id] = $domain->region_name;
  }
  $result = db_query('SELECT * FROM {domain_country} ORDER BY country');
  while ($country = db_fetch_object($result)) {
    $form['country_region_mapping'][$country->code] = array(
      '#title' => $country->country,
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['country_region_mapping'][$country->code][$country->code . '-mapping'] = array(
      '#title' => t('Region mapping'),
      '#type' => 'select',
      '#options' => $domain_options,
      '#default_value' => $country->domain_id,
    );
    $form['country_region_mapping'][$country->code][$country->code . '-enabled'] = array(
      '#title' => t('Enabled'),
      '#type' => 'checkbox',
      '#default_value' => $country->enabled,
    );
  }
  $form['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );
  $form['#submit'] = array('domain_country_settings_form_submit');
  return $form;
}

/**
 * Submit handler for the domain_country_settings form.
 */
function domain_country_settings_form_submit($form, &$form_state) {
  if ($current = variable_get('tek_client_geoip_default_domain_name', '') != $form_state['values']['default_domain_name']) {
    // Update the variable.
    variable_set('tek_client_geoip_default_domain_name', $form_state['values']['default_domain_name']);
    db_query("UPDATE {domain_country} SET region_name = '%s' WHERE domain_id = 0", $form_state['values']['default_domain_name']);
  }
  foreach ($form_state['values'] as $key => $value) {
    if ($string_length = strpos($key, '-mapping')) {
      $country_code = substr($key, 0, $string_length);
      if ($value != $form['country_region_mapping'][$country_code][$key]['#default_value']) {
        $region_name = domain_country_domain_to_region($value);
        $result = db_query("UPDATE {domain_country} SET domain_id = %d, region_name = '%s' WHERE code = '%s'", array(
          ':domain_id' => $value,
          ':region_name' => $region_name,
          ':code' => $country_code,
        ));
      }
    }
    elseif ($string_length = strpos($key, '-enabled')) {
      $country_code = substr($key, 0, $string_length);
      if ($value != $form['country_region_mapping'][$country_code][$key]['#default_value']) {
        $result = db_query("UPDATE {domain_country} SET enabled = %d WHERE code = '%s'", array(
          ':enabled' => $value,
          ':code' => $country_code,
        ));
      }
    }
  }
  if (isset($result)) {
    drupal_set_message(t('The changes have been saved.'));
  }
  else {
    drupal_set_message(t('There were no changes.'), 'warning');
  }
}

/**
 * Form-building function for import page.
 */
function domain_country_import() {
  $form = array();
  $form['import_file'] = array(
    '#type' => 'file',
    '#title' => t('Import CSV mapping file'),
    '#maxlength' => 40,
    '#description' => t('If you don\'t have direct file access to the server, use this field to upload a csv with country/domain mapping. See the default mapping "country_domain.csv" file in this module\'s directory for an example of the proper format.'),
  );
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 * Import initial data into the country list table.
 */
function domain_country_import_submit(&$form, &$form_state) {
  if ($file = file_save_upload('import_file', array('file_validate_extensions' => array('csv')))) {
    db_query("DELETE FROM {domain_country}");
    // Parse the CSV and import all countries and their mapping.
    if (($handle = fopen($file->filepath, 'r')) !== FALSE) {
      $header = TRUE;
      while (($row = fgetcsv($handle, 1000, ',')) !== FALSE) {
        if ($header) {
          // First row is the header; skip it.
          $header = FALSE;
          continue;
        }
        $record = array(
          'code' => strtolower($row[0]),
          'country' => ucwords(strtolower($row[1])),
          'region_name' => $row[2],
          'domain_id' => $row[3],
          'enabled' => $row[4],
        );
        $result = drupal_write_record('domain_country', $record);
      }
      fclose($handle);
      drupal_set_message('The country mapping was imported.');
    }
  }
}
